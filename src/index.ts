export { Provider as ReduxStoreProvider } from 'react-redux';

export { default as ConditionalRouter } from './ConditionalRouter';
export { navigate, getCurrentDomainNameForMultiDomainsApp } from './ConditionalRouter/navigator';
export { TAddRoutesType, IUrlProvider, IRoutingInfo } from './ConditionalRouter/types';
export { default as NavLink, INavLinkProps, resolveUrl } from './ConditionalRouter/NavLink';

export { default as IOftenHistoryPushDetectionSettings } from './ConditionalRouter/types/IOftenHistoryPushDetectionSettings';
export { default as withRouterReducer } from './ConditionalRouter/withRouterReducer';
import { navigator } from './ConditionalRouter/navigator';
export default navigator as any;
