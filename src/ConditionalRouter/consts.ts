export const DOMAIN_NAME_QUERY_PARAMETER = 'domainName';

export const SAME_PATH_ROUTES_SET_SYMBOL: symbol = Symbol('same path routes set');
