import React, { ComponentType } from 'react';

interface INextComponentResolver {
  componentsTree: ComponentType[];
  currentComponentIndex?: number;
}

const NextComponentResolver = ({ componentsTree, currentComponentIndex = 0 }: INextComponentResolver) => {
  if (!componentsTree || !componentsTree.length) return null;

  const Component = componentsTree[currentComponentIndex];

  if (currentComponentIndex === componentsTree.length - 1) {
    return <Component />;
  }

  return (
    <Component>
      <NextComponentResolver componentsTree={componentsTree} currentComponentIndex={currentComponentIndex + 1} />
    </Component>
  );
};

export default NextComponentResolver;
