import { IAnyObject, IRoutingInfo, IAction } from './types';
import { UPDATE_ROUTING_INFO } from '.';

const getDefaultState = (): IRoutingInfo => ({
  url: '',
  domainName: '',
  parameters: {},
  path: '',
  visitedRoutes: {},
  previousUrl: '',
  lockedRoutes: {},
  currentCheckpointUrl: '',
});

const routingReducer = (state: IRoutingInfo = getDefaultState(), action: IAction): IRoutingInfo => {
  if (action && action.type === UPDATE_ROUTING_INFO) {
    return { ...state, ...action.payload as IRoutingInfo };
  }
  return state;
};

const withRouterReducer = (reducersMapObject: IAnyObject) => {
  return { ...reducersMapObject, routingInfo: routingReducer };
};

export default withRouterReducer;
