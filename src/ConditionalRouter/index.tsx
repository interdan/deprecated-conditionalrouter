import React, { ComponentType } from 'react';
import { connect } from 'react-redux';
import withReduxContext from './withReduxContext';
import RouteResolver from './RouteResolver';
import {
  ILocation, IHistory, IRoutingInfo, TSetRulesType, TSwitchStateActionCreator,
  IResolveComponentResult, IStoreRequiredProps, IAnyObject, TGlobalOnRoute, ILocationWithHash,
} from './types';
import { setUseHttpsForMultiDomainsApp } from './globalConfig';
import NextComponentResolver from './NextComponentResolver';
import NavigationHistory from './utils/NavigationHistory';
import IOftenHistoryPushDetectionSettings from './types/IOftenHistoryPushDetectionSettings';

const { Provider: HistoryProvider, Consumer: HistoryConsumer } = React.createContext({
  // tslint:disable-next-line: no-empty
  navigate: (path: string) => { },
  // tslint:disable-next-line: no-empty
  replace: (path: string) => { },
});

export { HistoryConsumer };

const HISTORY_PUSH_ACTION = 'PUSH';
const HISTORY_REPLACE_ACTION = 'REPLACE';

interface IConditionalRouterProps {
  history: IHistory;
  setRules: TSetRulesType;
  onRoute?: TGlobalOnRoute;
  useHttpsForMultiDomainsApp?: boolean;
  isMultiDomainEmulating?: boolean;
  hashRouteMode?: boolean;
  switchStateActionCreator?: TSwitchStateActionCreator;
  oftenHistoryPushDetection?: IOftenHistoryPushDetectionSettings;
}

interface INotConnectedConditionalRouterProps extends IConditionalRouterProps, IStoreRequiredProps {
  routingInfo: IRoutingInfo;
}

const NAVIGATION_HISTORY_MAX_LENGTH = 10;
const HISTORY_ITEMS_COUNT_TO_TEST_OFFSET = 10;
const MIN_ACCEPTABLE_MILLISECONDS_OFFSET_FOR_HISTORY_PUSH_X_TIMES = 3000;

class ConditionalRouter extends React.PureComponent<INotConnectedConditionalRouterProps> {
  private routeResolver!: RouteResolver;
  private navigationHistory?: NavigationHistory;
  private unsubscribeFromHistory: () => void;
  private currentComponentsTree: ComponentType[];
  private toMuchHistoryPushNotified = false;

  private currentRules!: TSetRulesType;

  public static defaultProps = {
    switchStateActionCreator: null,
    isMultiDomainEmulating: false,
    useHttpsForMultiDomainsApp: true,
    hashRouteMode: false,
  };

  constructor(props: INotConnectedConditionalRouterProps) {
    super(props);

    const { history, oftenHistoryPushDetection } = this.props;

    if (oftenHistoryPushDetection) {
      const { historyMaxLength = NAVIGATION_HISTORY_MAX_LENGTH } = oftenHistoryPushDetection;
      this.navigationHistory = new NavigationHistory(historyMaxLength);
    }

    setUseHttpsForMultiDomainsApp(props.useHttpsForMultiDomainsApp as boolean);

    // listen to any url update
    this.unsubscribeFromHistory = history.listen(this.resolveRoute);

    this.initRouteResolver();

    // init app with empty component
    this.currentComponentsTree = [];
  }

  public componentDidMount() {
    this.resolveRoute();
  }

  public componentDidUpdate() {
    const { setRules, routingInfo: { path } } = this.props;

    const setRulesUpdatedWithHMR = this.currentRules !== setRules;
    if (setRulesUpdatedWithHMR) {
      this.initRouteResolver();
      this.resolveRoute();
    } else {
      const needToInitState = !path;
      if (needToInitState) {
        this.resolveRoute();
      }
    }
  }

  public componentWillUnmount() {
    this.unsubscribeFromHistory();
  }

  private initRouteResolver = (): void => {
    const { setRules, store, isMultiDomainEmulating,
      switchStateActionCreator, onRoute, hashRouteMode = false } = this.props;

    this.currentRules = setRules;

    const { getState, dispatch } = store;
    this.routeResolver = new RouteResolver(
      setRules, getState, dispatch, this.navigate,
      !!isMultiDomainEmulating, hashRouteMode,
      switchStateActionCreator, onRoute,
    );
  }

  private navigate = (...args): void => {
    (this.props.history as any).push(...args);

    if (!this.props.oftenHistoryPushDetection
      || this.toMuchHistoryPushNotified) return;

    const path = args[0] || null;
    this.navigationHistory!.add(path);

    const { notifyTooMuchHistoryPush,
      historyItemsCountToCheckOftenPush = HISTORY_ITEMS_COUNT_TO_TEST_OFFSET,
      minAcceptableMillisecondsOffsetForPushXItemToHistory = MIN_ACCEPTABLE_MILLISECONDS_OFFSET_FOR_HISTORY_PUSH_X_TIMES } = this.props.oftenHistoryPushDetection;

    if (notifyTooMuchHistoryPush) {
      const navItem = this.navigationHistory!.getItemByQueueEndIndex(historyItemsCountToCheckOftenPush);
      if (navItem) {
        const millisecondsDifference = new Date().getTime() - navItem.time.getTime();
        if (millisecondsDifference < minAcceptableMillisecondsOffsetForPushXItemToHistory) {
          const navHistory = this.navigationHistory!.getNavigationHistory();
          notifyTooMuchHistoryPush({
            navHistory,
            millisecondsDifference,
            navEventsCount: HISTORY_ITEMS_COUNT_TO_TEST_OFFSET,
          });
          this.toMuchHistoryPushNotified = true;
        }
      }
    }
  }

  private resolveLocation(location: ILocationWithHash): ILocation {
    const { hashRouteMode } = this.props;

    if (hashRouteMode) {
      const hash = location.hash || '';

      const urlParts = hash.split('?');

      const pathname = urlParts.length ? urlParts[0].substring(1) : '';
      const search = urlParts.length > 1 ? `?${urlParts[1]}` : '';

      return { pathname, search };
    }

    return location;
  }

  private resolveRoute = (
    location: ILocationWithHash = this.props.history.location,
    action: string = HISTORY_PUSH_ACTION,
  ): void => {
    if (action === HISTORY_REPLACE_ACTION) return;

    const { pathname, search } = this.resolveLocation(location);
    const { componentsTree, redirectTo, newRoutingInfo, callBacks }: IResolveComponentResult = this.routeResolver
      .resolveComponent(this.props.routingInfo, pathname, search);

    if (redirectTo) {
      this.props.history.replace(redirectTo);
    }

    this.currentComponentsTree = componentsTree;

    if (callBacks.length) {
      callBacks.forEach(callBack => callBack());
    }
    this.setRoutingInfo(newRoutingInfo);
  }

  private setRoutingInfo(newRoutingInfo: IRoutingInfo) {
    this.props.store.dispatch({ type: UPDATE_ROUTING_INFO, payload: newRoutingInfo });
  }

  public render() {
    return (
      <HistoryProvider value={{ navigate: this.navigate, replace: this.props.history.replace }}>
        <NextComponentResolver componentsTree={this.currentComponentsTree} />
      </HistoryProvider>
    );
  }
}

export const UPDATE_ROUTING_INFO = 'UPDATE_ROUTING_INFO';

const RouterWithReduxContext = withReduxContext(ConditionalRouter as any as ComponentType<IStoreRequiredProps>);

const mapStateToProps = (state: IAnyObject) => ({ routingInfo: state.routingInfo });
const ResultComponent: ComponentType<IConditionalRouterProps> = connect(mapStateToProps)(RouterWithReduxContext) as any;
export default ResultComponent;
