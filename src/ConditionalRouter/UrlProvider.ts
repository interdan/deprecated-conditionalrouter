import { IUrlProvider, TNavigate } from './types';
import resolveFinalUrl from './utils/resolveFinalUrl';

class UrlProvider implements IUrlProvider {
  protected urlValue: string;
  protected navigate: TNavigate;

  constructor(url: string, navigate: TNavigate) {
    this.urlValue = url;
    this.navigate = navigate;
  }

  public get url(): string {
    return this.urlValue;
  }

  public go = (): void => this.navigate(resolveFinalUrl(this.url));
}

export default UrlProvider;
