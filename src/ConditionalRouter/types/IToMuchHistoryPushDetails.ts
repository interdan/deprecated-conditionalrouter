import INavigationHistoryItem from './INavigationHistoryItem';

interface IToMuchHistoryPushDetails {
  navHistory: INavigationHistoryItem[];
  millisecondsDifference: number;
  navEventsCount: number;
}

export default IToMuchHistoryPushDetails;
