import IStringValues from './IStringValues';

interface IMatchRoutePath {
  params: IStringValues;
}

export default IMatchRoutePath;
