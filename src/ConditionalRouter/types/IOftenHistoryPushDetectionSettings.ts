import IToMuchHistoryPushDetails from './IToMuchHistoryPushDetails';

interface IOftenHistoryPushDetectionSettings {
  notifyTooMuchHistoryPush: (details: IToMuchHistoryPushDetails) => void;

  historyMaxLength?: number;
  historyItemsCountToCheckOftenPush?: number;
  minAcceptableMillisecondsOffsetForPushXItemToHistory?: number;
}

export default IOftenHistoryPushDetectionSettings;
