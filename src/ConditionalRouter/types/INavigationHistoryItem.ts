interface INavigationHistoryItem {
  path: string;
  millisecondOffset: number;
}

export default INavigationHistoryItem;
