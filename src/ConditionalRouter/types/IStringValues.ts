interface IStringValues {
  [key: string]: string;
}

export default IStringValues;
