import { initNavigator, navigator, getCurrentDomainNameForMultiDomainsApp, addNavigatorValue } from './navigator';

import { IRoute, TNavigate, TGetState, TSetRulesType, TAddRoutesParameter, TAddRoutesType, IRouteRule, IReactComponentInfo } from './types';
import { ComponentType } from 'react';
import { SAME_PATH_ROUTES_SET_SYMBOL } from './consts';
import findReactComponent from './utils/findReactComponent';
import createNavigatorValue from './utils/createNavigatorValue';

const defaultRouteOptions: IRouteRule = {
  path: '',
  domainName: '',
  useAsDefault: false,
  requiredStep: false,
  checkIsAvailable: null,
  alwaysAccessibleRoute: false,
  beforeRoute: null,
  onRoute: null,
  customSettings: {},
  skipWhenBackToPrevious: false,
  routesToLockOnFirstLoad: {},
};

interface ILastRequiredStep {
  [domainName: string]: string;
}

class RoutingsRulesReader {
  private navigate: TNavigate;
  private isMultiDomainEmulating: boolean;
  private currentContainers: ComponentType[];
  private defaultRoute!: IRoute;
  private multiDomains: boolean;
  private lastDomainName: string;
  private lastRequiredStepRoutePath: ILastRequiredStep;
  private routes: IRoute[];
  private hashRouteMode: boolean;

  constructor(navigate: TNavigate, isMultiDomainEmulating: boolean, hashRouteMode: boolean) {
    this.navigate = navigate;
    this.isMultiDomainEmulating = isMultiDomainEmulating;
    this.currentContainers = [];
    this.multiDomains = false;
    this.lastDomainName = '';
    this.lastRequiredStepRoutePath = {};
    this.routes = [];
    this.hashRouteMode = hashRouteMode;
  }

  public read(setRules: TSetRulesType, getState: TGetState): {
    routes: IRoute[],
    defaultRoute: IRoute,
    multiDomains: boolean,
  } {
    initNavigator(this.navigate, getState, () => this.defaultRoute, this.isMultiDomainEmulating, this.hashRouteMode);

    setRules(this.addRoutes, navigator);

    return { routes: this.routes, defaultRoute: this.defaultRoute, multiDomains: this.multiDomains };
  }

  public registerRoute = (route: IRoute): void => {
    this.routes.push(route);

    const { path } = route;
    let routesForThisPath: any = this.routes[path as any];

    if (routesForThisPath) {
      // current value is routes set
      if (!routesForThisPath[SAME_PATH_ROUTES_SET_SYMBOL]) {
        // create a new routes set with domainName as a key
        // Symbol mark will garantee the ability to differ among the regular route
        routesForThisPath = {
          [SAME_PATH_ROUTES_SET_SYMBOL]: true,
          [routesForThisPath.domainName || '']: routesForThisPath,
        };
        this.routes[path as any] = routesForThisPath;
      }

      routesForThisPath[route.domainName || ''] = route;
    } else {
      this.routes[path as any] = route;
    }
  }

  private getDefaultRouteDomainName = (): string => (this.defaultRoute ? this.defaultRoute.domainName : '');

  private addRoute = (routeObject: IRouteRule) => {
    const { componentName, component }: IReactComponentInfo = findReactComponent(routeObject);

    const { path, skipWhenBackToPrevious, onRoute, beforeRoute, useAsDefault, requiredStep, domainName,
      checkIsAvailable, routesToLockOnFirstLoad, alwaysAccessibleRoute, customSettings } = { ...defaultRouteOptions, ...routeObject };

    this.multiDomains = this.multiDomains || !!domainName;

    const domainNameToSet: string = (domainName || this.lastDomainName).toLowerCase();
    this.lastDomainName = domainNameToSet;

    const routesToLock: string[] = Object.keys(routesToLockOnFirstLoad as object)
      .map(routeName => {
        const route = this.routes.find(({ name }) => name === routeName);
        if (!route) {
          throw new Error(`Error of adding ${componentName} route: some of routes to lock weren't found!`);
        }
        return route.path;
      });

    const currentDomainName = domainNameToSet || getCurrentDomainNameForMultiDomainsApp('', this.multiDomains, this.getDefaultRouteDomainName());
    const requiredVisitedRoute = this.lastRequiredStepRoutePath[currentDomainName] || '';

    const routeUrl = path || `/${componentName}`;
    const [pathName, searchString] = routeUrl.split('?');

    const routeItem: IRoute = {
      requiredVisitedRoute,
      routesToLock,
      requiredStep: requiredStep!,
      beforeRoute: beforeRoute!,
      onRoute: onRoute!,
      customSettings: customSettings!,
      alwaysAccessibleRoute: alwaysAccessibleRoute!,
      skipWhenBackToPrevious: skipWhenBackToPrevious!,
      path: pathName,
      name: componentName,
      componentsTree: [...this.currentContainers, component],
      domainName: domainNameToSet,
      checkIsAvailable: this.routes.length > 0 ? checkIsAvailable! : null,
    };

    const firstDomainOccured = domainName && !this.multiDomains;
    if (firstDomainOccured && this.routes.length) {
      throw new Error(`In multi domains app exists route without domain specification: "${this.routes[0].name}" !`);
    }

    if (useAsDefault) {
      // added route is used as default for any wrong url (page 404)
      // must be called no more then one time
      if (this.defaultRoute) {
        throw new Error(`Detected the attempt of adding more then one route as default!
    Current default route path is "${this.defaultRoute.path}".
    New default route path is "${routeItem.path}".
    Only one route can be added as default!`);
      }
      this.defaultRoute = routeItem;
    }

    this.registerRoute(routeItem);

    addNavigatorValue(
      componentName,
      domainNameToSet,
      createNavigatorValue(
        this.multiDomains, this.navigate, pathName, searchString,
        this.isMultiDomainEmulating, this.getDefaultRouteDomainName,
        domainNameToSet, getCurrentDomainNameForMultiDomainsApp,
        this.hashRouteMode,
      ),
      useAsDefault!,
    );

    if (requiredStep) {
      this.lastRequiredStepRoutePath[domainNameToSet] = routeItem.path;
    }
  }

  private pushParentContainer = (container: ComponentType): void => { this.currentContainers.push(container); };

  private popParentContainer = (): void => {
    if (this.currentContainers.length) {
      this.currentContainers.pop();
    }
  }

  private addRoutes: TAddRoutesType = (...args: TAddRoutesParameter[]): void => {
    if (!args.length) return;

    const lastParameter = args.pop();
    const routesArray: IRouteRule[] = Array.isArray(lastParameter) ?
      lastParameter as IRouteRule[] :
      [lastParameter as IRouteRule];

    const parentContainers = args as ComponentType[];
    parentContainers.forEach(item => this.pushParentContainer(item));

    routesArray.forEach(route => this.addRoute(route));

    // remove added parent containers
    parentContainers.forEach(() => this.popParentContainer());
  }
}

export default RoutingsRulesReader;
