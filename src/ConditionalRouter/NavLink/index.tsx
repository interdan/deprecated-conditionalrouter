import React, { ReactNode } from 'react';
import { HistoryConsumer } from '../';
import { TNavigate } from '../types';
import resolveFinalUrl from '../utils/resolveFinalUrl';

type TUrlValue = string | { url: string } | (() => { url: string });

export interface INavLinkProps {
  [key: string]: ReactNode | string | boolean;
  children: ReactNode;
  to: TUrlValue;
  disabled?: boolean;
  replace?: boolean;
}

export const resolveUrl = (urlValue: TUrlValue): string => {
  switch (typeof urlValue) {
    case 'string':
      return urlValue;

    case 'object':
      return urlValue.url;

    case 'function':
      return urlValue().url;

    default:
      throw new Error('"to" prop must have a valid route: string or navigator value!');
  }
};

const NavLink = ({ disabled = false, replace = false, to, children, ...rest }: INavLinkProps) => {

  const onClick = (e, navigate: TNavigate, historyReplace: TNavigate) => {
    if (e && e.preventDefault) {
      e.preventDefault();
    }

    if (typeof rest.onClick === 'function') {
      rest.onClick(e);
    }

    if (!disabled && to) {
      const newUrl = resolveUrl(to);
      (replace ? historyReplace : navigate)(resolveFinalUrl(newUrl));
    }
  };

  return (
    <HistoryConsumer>
      {({ navigate, replace: historyReplace }) => {
        return (
          <a
            {...rest}
            onClick={e => onClick(e, navigate, historyReplace)}
            href={resolveUrl(to)}
          >
            {children}
          </a>
        );
      }}
    </HistoryConsumer>
  );
};

export default NavLink;
