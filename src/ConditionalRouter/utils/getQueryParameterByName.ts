const getSearchFromLocation = (hashRouteMode: boolean): string => {
  if (hashRouteMode) {
    const searchFromHash = (window.location.hash || '').split('?')[1];
    if (searchFromHash) {
      return `?${searchFromHash}`;
    }
  }

  return window.location.search;
};

export default function getQueryParameterByName(queryParameterName: string, hashRouteMode: boolean, _search?: string): string  {
  const search = _search || getSearchFromLocation(hashRouteMode);
  if (!search) return '';

  return search.substring(1).split('&').reduce(
    (result: string, current: string) => {
      if (result) return result;

      const [parameterName, parameterValue] = current.split('=');
      return decodeURIComponent(parameterName) === queryParameterName ?
        decodeURIComponent(parameterValue) :
        '';
    },
    '',
  );
}
