import UrlProvider from '../UrlProvider';
import MultiDomainsUrlProvider from '../MultiDomainsUrlProvider';
import { IUrlProvider, TNavigate, TGetCurrentDomainName } from '../types';

export default function createUrlProvider(
  multiDomains: boolean,
  navigate: TNavigate,
  url: string,
  domainName: string,
  isMultiDomainEmulating: boolean,
  getCurrentDomain: TGetCurrentDomainName,
  getDefaultRouteDomainName: () => string,
  hashRouteMode: boolean = false,
): IUrlProvider {
  // todo: add multidomain support
  const resultUrl = hashRouteMode ? `#${url}` : url;

  return multiDomains ?

    new MultiDomainsUrlProvider(
      resultUrl,
      navigate,
      domainName,
      isMultiDomainEmulating,
      () => getCurrentDomain('', true, getDefaultRouteDomainName()),
    ) :

    new UrlProvider(resultUrl, navigate);
}
