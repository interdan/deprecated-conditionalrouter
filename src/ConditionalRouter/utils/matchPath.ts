import pathToRegexp from 'path-to-regexp';
import IMatchRoutePath from '../types/IMatchRoutePath';

const cache = {};
const cacheLimit = 10000;
let cacheCount = 0;

const pathToRegExpOptions = {
  end: true,
  strict: false,
  sensitive: false,
};

const compilePath = (path: string) => {
  const existedPath = cache[path];
  if (existedPath) return existedPath;

  const keys = [];
  const regexp = pathToRegexp(path, keys, pathToRegExpOptions);
  const result = { regexp, keys };

  if (cacheCount < cacheLimit) {
    cache[path] = result;
    cacheCount += 1;
  }

  return result;
};

export default function matchPath(pathname: string, routePath: string): IMatchRoutePath | null {
  const { regexp, keys } = compilePath(routePath);
  const match = regexp.exec(pathname);

  if (!match) return null;

  const [url, ...values] = match;
  if (pathname !== url) return null;

  const params = keys.reduce(
    (resultParams, key, index: number) => {
      resultParams[key.name] = values[index];
      return resultParams;
    },
    {},
  );

  return { params };
}
