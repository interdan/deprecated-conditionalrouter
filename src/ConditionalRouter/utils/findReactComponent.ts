import { ComponentType } from 'react';
import { IRouteRule, IReactComponentInfo } from '../types';

const startsFromUppercase = (str: string) => {
  const firstChar = str.charAt(0);
  return firstChar !== firstChar.toLowerCase();
};

export default function findReactComponent(routeObject: IRouteRule): IReactComponentInfo {
  const componentKeyValue = Object.entries(routeObject).find(([key, value]) => {
    if (!startsFromUppercase(key)) return false;

    return typeof value === 'function' || typeof value === 'object';
  });

  if (!componentKeyValue) {
    throw new Error('Route must containe the component, his name must be started with an upper case letter, provided route is wrong');
  }

  const [componentName, component] = componentKeyValue;
  return { componentName, component: component as ComponentType };
}
