import pathToRegexp from 'path-to-regexp';
import { TNavigate, TGetCurrentDomainName, TNavigatorValue } from '../types';
import createUrlProvider from './createUrlProvider';

export default function tryBuildGetPathFunction(
  multiDomains: boolean,
  navigate: TNavigate,
  pathName: string,
  searchString: string,
  domainName: string,
  isMultiDomainEmulating: boolean,
  getCurrentDomain: TGetCurrentDomainName,
  getDefaultRouteDomainName: () => string,
  hashRouteMode: boolean,
): TNavigatorValue | null {
  const pathParameters: any[] = [];
  pathToRegexp(pathName, pathParameters);

  const searchParameters: any[] = [];
  pathToRegexp(searchString || '', searchParameters);

  if (pathParameters.length === 0 && searchParameters.length === 0) return null;

  const buildPathWithNamedParameters = pathToRegexp.compile(pathName);
  return (urlParameters: any = {}) => {
    let resultPath = buildPathWithNamedParameters(urlParameters);
    if (searchParameters.length > 0) {
      const queryParameters = searchParameters.map(({ name }: any): string | null => {
        const value = urlParameters[name];
        if (value === '' || value == null) return null;

        return `${name}=${encodeURIComponent(value)}`;

      }).filter(Boolean);

      resultPath += `?${queryParameters.join('&')}`;
    }
    return createUrlProvider(
      multiDomains, navigate, resultPath, domainName,
      isMultiDomainEmulating, getCurrentDomain, getDefaultRouteDomainName,
      hashRouteMode,
    );
  };
}
