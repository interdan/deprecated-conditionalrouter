import { IRoute } from '../types';
import { SAME_PATH_ROUTES_SET_SYMBOL } from '../consts';

export default function getRouteByPath(routes: IRoute[], path: string, domain: string): IRoute  {
  const route = routes[path as any] as any;

  return route[SAME_PATH_ROUTES_SET_SYMBOL] ?
    route[domain] :
    route;
}
