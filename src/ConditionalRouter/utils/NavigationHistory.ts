import INavigationHistoryItem from '../types/INavigationHistoryItem';

interface INavigationEvent {
  time: Date;
  path: string;
}

class NavigationHistory {
  private history!: INavigationEvent[];
  private lastAddedItemIndex: number = -1;

  constructor(historyMaxLength) {
    // tslint:disable-next-line: prefer-array-literal
    this.history = new Array(parseInt(historyMaxLength, 10));
  }

  public add = (path: string): void => {
    this.lastAddedItemIndex += 1;

    if (this.lastAddedItemIndex === this.history.length) {
      this.lastAddedItemIndex = 0;
    }

    this.history[this.lastAddedItemIndex] = {
      path,
      time: new Date(),
    };
  }

  public getItemByQueueEndIndex = (queueEndIndex: number = this.history.length): (INavigationEvent | null) => {
    if (queueEndIndex > this.history.length || queueEndIndex < 0) {
      throw new Error(`NavigationHistory: queueEndIndex (${queueEndIndex}) can't be more then historyMaxLength (${this.history.length})!`);
    }

    if (this.lastAddedItemIndex === -1) {
      // the history is empty
      return null;
    }

    let itemToReturnIndex = this.lastAddedItemIndex - queueEndIndex;
    if (itemToReturnIndex < 0) {
      itemToReturnIndex = this.history.length + itemToReturnIndex;
    }

    return this.history[itemToReturnIndex];
  }

  public getNavigationHistory = (): INavigationHistoryItem[] => {
    const result: INavigationHistoryItem[] = [];

    const currentTimeMilliseconds: number = new Date().getTime();
    let index = this.lastAddedItemIndex;

    while (true) {
      const currentItem = this.history[index];
      if (!currentItem) {
        // history isn't used all slots
        break;
      }

      const { time, path } = currentItem;
      const millisecondOffset = currentTimeMilliseconds - time.getTime();
      result.push({ path, millisecondOffset });

      index -= 1;
      if (index === this.lastAddedItemIndex) {
        // we've made a full cycle
        break;
      }

      if (index === -1) {
        index = this.history.length - 1;
      }
    }

    return result;
  }

  public toString = () => JSON.stringify(this.getNavigationHistory());
}

export default NavigationHistory;
