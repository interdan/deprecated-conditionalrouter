import { TNavigate, TGetCurrentDomainName, TNavigatorValue } from '../types';
import tryBuildGetPathFunction from './tryBuildGetPathFunction';
import createUrlProvider from './createUrlProvider';

export default function createNavigatorValue(
  multiDomains: boolean,
  navigate: TNavigate,
  pathName: string,
  search: string,
  isMultiDomainEmulating: boolean,
  getDefaultRouteDomainName: () => string,
  domainName: string,
  getCurrentDomain: TGetCurrentDomainName,
  hashRouteMode: boolean,
): TNavigatorValue {

  let navigatorValue: TNavigatorValue | null;
  navigatorValue = tryBuildGetPathFunction(
    multiDomains, navigate, pathName, search, domainName,
    isMultiDomainEmulating, getCurrentDomain, getDefaultRouteDomainName,
    hashRouteMode,
  );

  if (!navigatorValue) {
    const staticValue = createUrlProvider(
      multiDomains, navigate, pathName, domainName,
      isMultiDomainEmulating, getCurrentDomain, getDefaultRouteDomainName,
      hashRouteMode,
    );

    navigatorValue = () => staticValue;
  }

  return navigatorValue!;
}
