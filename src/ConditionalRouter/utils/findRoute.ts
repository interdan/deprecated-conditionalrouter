import queryString from 'qs';
import matchPath from './matchPath';
import { IRoute, IRouteMatch } from '../types';
import { SAME_PATH_ROUTES_SET_SYMBOL } from '../consts';
import IMatchRoutePath from '../types/IMatchRoutePath';

export default function findRoute(
  routes: IRoute[],
  pathname: string,
  search: string,
  domainName: string): IRouteMatch | null {

  for (const { path } of routes) {
    const routesWithSamePath = routes[path as any] as any;

    const route = routesWithSamePath[SAME_PATH_ROUTES_SET_SYMBOL as any] ?
      routesWithSamePath[domainName] :
      routesWithSamePath;

    if (!route) continue;

    const match: IMatchRoutePath | null = matchPath(pathname, route.path);
    if (match) {
      const params = { ...queryString.parse(search.substring(1)), ...match.params };
      return { route, params };
    }
  }

  return null;
}
