import { DOMAIN_NAME_QUERY_PARAMETER } from '../consts';

const escapeRegExp = (val: string): string => val.replace(/[.*+?^${}()|[\]\\]/g, '\\$&');

export default function buildRedirectUrl(
  redirectRoutePath: string | undefined,
  multiDomains: boolean,
  isMultiDomainEmulating: boolean,
  currentDomainName: string,
) {
  if (!redirectRoutePath || !multiDomains || !isMultiDomainEmulating) return redirectRoutePath;

  const suffix = `${DOMAIN_NAME_QUERY_PARAMETER}=${currentDomainName}`;

  const alreadyContainsThisDomainName = redirectRoutePath.search(new RegExp(`[\?,&]${suffix}`)) !== -1;
  if (alreadyContainsThisDomainName) return redirectRoutePath;

  const parameterPrefix = redirectRoutePath.indexOf('?') >= 0 ? '&' : '?';

  return redirectRoutePath + parameterPrefix + suffix;
}
