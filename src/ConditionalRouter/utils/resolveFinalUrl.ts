export default function resolveFinalUrl(builtUrl: string): string {
  if (builtUrl.startsWith('#')) {
    const { pathname, search } = window.location;
    return `${pathname}${search}${builtUrl}`;
  }

  return builtUrl;
}
