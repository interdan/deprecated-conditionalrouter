import { ComponentType, ReactNode } from 'react';
import { Dispatch, Action } from 'redux';
import IStringValues from './types/IStringValues';

export interface IAnyObject {
  [key: string]: any;
}

export interface IAction {
  type: string;
  payload?: IAnyObject | null;
}

export interface IUrlProvider {
  readonly url: string;
  go: () => void;
}

export type TOnRouteFunction = (dispatch?: Dispatch, getState?: TGetState) => (params?: IStringValues) => void;
export type TOnRoute = TOnRouteFunction | null;

export type TGlobalOnRouteFunction = (dispatch?: Dispatch, getState?: TGetState) => (params?: IStringValues, customSettings?: IAnyObject) => void;
export type TGlobalOnRoute = TGlobalOnRouteFunction | null;

export type TGetState = () => IAnyObject;

type TBeforeRouteFunction = (getState: TGetState, dispatch: Dispatch) => (params: IStringValues) => IUrlProvider;
export type TBeforeRoute = TBeforeRouteFunction | null;

type TCheckIsAvailableFunction = (state: IAnyObject) => boolean;
type TCheckIsAvailable = TCheckIsAvailableFunction | null;

export interface IRoute {
  componentsTree: ComponentType[];
  path: string;
  name: string;
  onRoute: TOnRoute;
  beforeRoute: TBeforeRoute;
  requiredStep: boolean;
  requiredVisitedRoute: string;
  domainName: string;
  checkIsAvailable: TCheckIsAvailable;
  alwaysAccessibleRoute: boolean;
  routesToLock: string[];
  customSettings?: IAnyObject;
  skipWhenBackToPrevious: boolean;
}

export interface IRouteByDomain {
  [domainName: string]: IRoute;
}

interface IComponentsCollection {
  [key: string]: ComponentType;
}

export interface IRouteRule {
  [key: string]: any;

  path?: string;
  domainName?: string;
  useAsDefault?: boolean;
  requiredStep?: boolean;
  alwaysAccessibleRoute?: boolean;
  checkIsAvailable?: TCheckIsAvailable;
  beforeRoute?: TBeforeRoute;
  skipWhenBackToPrevious?: boolean;
  onRoute?: TOnRoute;
  routesToLockOnFirstLoad?: IComponentsCollection;
  customSettings?: any;
}

export interface IRouteMatch {
  route: IRoute;
  params: IStringValues;
}

export interface IReactComponentInfo {
  component: ComponentType;
  componentName: string;
}

export type TGetCurrentDomainName = (
  search: string,
  multiDomains: boolean,
  defaultRouteDomainName?: string,
  isMultiDomainEmulating?: boolean,
  isHashRouteModeUsed?: boolean,
) => string;

export type TNavigate = (path: string) => void;

export type TNavigatorValue = (...args: string[]) => IUrlProvider;

export interface INavigator {
  [key: string]: null | string | TNavigatorValue | (() => TNavigatorValue) | ((routeName: string) => void) | (() => void);
  getCurrent: () => TNavigatorValue;
  setCurrent: (routeName: string) => void;
  navigateToPreviousRoute: () => void;
  default: TNavigatorValue | null;
}

export interface IStore {
  getState: () => IAnyObject;
  dispatch: Dispatch;
}

export interface IStoreRequiredProps {
  store: IStore;
}

export type TAddRoutesParameter = ComponentType | IRouteRule | IRouteRule[];

export type TAddRoutesType = (...args: TAddRoutesParameter[]) => void;

export type TSetRulesType = (addRoutes: TAddRoutesType, navigator: INavigator) => void;

export type TSwitchStateActionCreator = (domainName: string) => IAction;

interface IVisitedRoutes {
  [routePath: string]: boolean;
}

interface ILockedRoutes {
  [routeName: string]: boolean;
}

export interface IRoutingInfo {
  url: string;
  path: string;
  parameters: IAnyObject;
  domainName: string;
  visitedRoutes: IVisitedRoutes;
  currentCheckpointUrl: string;
  lockedRoutes: ILockedRoutes;
  previousUrl?: string;
}

export interface ILocation {
  pathname: string;
  search: string;
}

export interface ILocationWithHash extends ILocation {
  hash: string;
}

type TOnListen = (location: ILocationWithHash, action: string) => void;

export interface IHistory {
  location: ILocationWithHash;
  push: TNavigate;
  replace: TNavigate;
  listen: (onListen: TOnListen) => () => void;
}

export interface IResolveComponentResult {
  componentsTree: ComponentType[];
  newRoutingInfo: IRoutingInfo;
  redirectTo?: string;
  callBacks: (() => void)[];
}
