let useHttps: boolean;

export const setUseHttpsForMultiDomainsApp = (useHttpsForMultiDomainsApp: boolean) => {
  useHttps = useHttpsForMultiDomainsApp;
};

export const getUseHttpsForMultiDomainsApp = () => useHttps;
