import {
  navigator,
  getCurrentDomainNameForMultiDomainsApp as getDomain,
} from './navigator';
import UrlProvider from './UrlProvider';
import MultiDomainsUrlProvider from './MultiDomainsUrlProvider';
import RoutingsRulesReader from './RoutingsRulesReader';
import { TNavigate, TSetRulesType, TGetState, TSwitchStateActionCreator, IRoute, IRoutingInfo, ILocation, IAnyObject, IResolveComponentResult, TGlobalOnRoute, IRouteMatch } from './types';
import buildRedirectUrl from './utils/buildRedirectUrl';
import { Dispatch, Action } from 'redux';
import findRoute from './utils/findRoute';
import getRouteByPath from './utils/getRouteByPath';

interface IResolveRouteResult {
  route: IRoute;
  params: IAnyObject;
  redirectTo?: string;
}

class RouteResolver {
  private getState: TGetState;
  private dispatch: Dispatch;
  private switchStateActionCreator?: TSwitchStateActionCreator;
  private previousDomainName: string;

  private defaultRoute: IRoute;
  private multiDomains: boolean;
  private isMultiDomainEmulating: boolean;
  private routes: IRoute[];

  private globalOnRoute?: TGlobalOnRoute;

  constructor(
    setRules: TSetRulesType,
    getState: TGetState,
    dispatch: Dispatch,
    navigate: TNavigate,
    isMultiDomainEmulating: boolean,
    hashRouteMode: boolean,
    switchStateActionCreator?: TSwitchStateActionCreator,
    globalOnRoute?: TGlobalOnRoute,
  ) {
    this.getState = getState;
    this.dispatch = dispatch;
    this.switchStateActionCreator = switchStateActionCreator;
    this.previousDomainName = '';

    const { routes, defaultRoute, multiDomains } = new RoutingsRulesReader(navigate, isMultiDomainEmulating, hashRouteMode)
      .read(setRules, getState);
    this.defaultRoute = defaultRoute;
    this.multiDomains = multiDomains;
    this.isMultiDomainEmulating = isMultiDomainEmulating;
    this.routes = routes;

    this.globalOnRoute = globalOnRoute;
  }

  public resolveComponent = (
    routingInfo: IRoutingInfo,
    pathname: string,
    search: string,
    callBacks: (() => void)[] = [],
  ): IResolveComponentResult => {
    const currentDomainName: string = getDomain(search, this.multiDomains, this.defaultRoute.domainName);
    const domainChanged = this.previousDomainName !== currentDomainName && !!this.previousDomainName;

    if (domainChanged) {
      // if domainChanged (it's possible in develop environment only):
      this.previousDomainName = currentDomainName;

      // we have to switch store
      if (!this.switchStateActionCreator) {
        throw new Error('switchStateActionCreator property must be provided for multidomain app!');
      }

      const switchDomainAction = this.switchStateActionCreator(currentDomainName);
      callBacks.push(this.dispatch.bind(null, switchDomainAction));

      return {
        ...this.resolveComponent(this.getState().routingInfo, pathname, search, callBacks),
      };
    }

    this.previousDomainName = currentDomainName;

    const routeMatch: IRouteMatch | null = findRoute(this.routes, pathname, search, currentDomainName);
    const { route, params, redirectTo: redirectRoutePath } = this.resolveRouteByPathName(routeMatch, currentDomainName, routingInfo);
    const { componentsTree, path, name, requiredStep, routesToLock, onRoute, customSettings } = route;

    const { visitedRoutes } = routingInfo;
    const routeVisitedFirstTime = !visitedRoutes[path];
    const isCheckpoint = !!routesToLock.length;
    const setAsCurrentCheckpoint = isCheckpoint && routeVisitedFirstTime;

    if (typeof this.globalOnRoute === 'function') {
      callBacks.push(this.globalOnRoute(this.dispatch, this.getState).bind(null, params, customSettings));
    }

    if (typeof onRoute === 'function') {
      callBacks.push(onRoute(this.dispatch, this.getState).bind(null, params));
    }

    let newVisitedRoutes = visitedRoutes;
    if (requiredStep && !newVisitedRoutes[path]) {
      newVisitedRoutes = { ...visitedRoutes, [path]: true };
    }

    const redirectTo = buildRedirectUrl(redirectRoutePath, this.multiDomains, this.isMultiDomainEmulating, currentDomainName);
    const url = redirectTo || (pathname + search);
    navigator.setCurrent(name);

    const currentCheckpointUrl = setAsCurrentCheckpoint ? url : routingInfo.currentCheckpointUrl;

    let { lockedRoutes } = routingInfo;
    if (setAsCurrentCheckpoint) {
      lockedRoutes = {
        ...routingInfo.lockedRoutes,
        ...routesToLock.reduce(
          (result: any, currentValue) => {
            result[currentValue] = true;
            return result;
          },
          {},
        ),
      };
    }

    const newRoutingInfo: IRoutingInfo = {
      url,
      path,
      lockedRoutes,
      currentCheckpointUrl,
      parameters: params,
      domainName: currentDomainName,
      visitedRoutes: newVisitedRoutes,
    };

    const previousRoute: IRouteMatch | null = this.findRouteByUrl(routingInfo.url, currentDomainName);
    if (!previousRoute || !previousRoute.route.skipWhenBackToPrevious) {
      newRoutingInfo.previousUrl = routingInfo.url;
    }

    return {
      redirectTo,
      newRoutingInfo,
      componentsTree,
      callBacks,
    };
  }

  private findRouteByUrl = (url: string, domainName: string): IRouteMatch | null => {
    if (!url) return null;

    const [pathName, search = ''] = url.split('?');
    return findRoute(this.routes, pathName, search ? `?${search}` : '', domainName);
  }

  private resolveRouteByPathName = (
    routeMatch: IRouteMatch | null,
    domainName: string,
    routingInfo: IRoutingInfo,
    redirectTo = '',
  ): IResolveRouteResult => {
    if (!routeMatch) {
      return { route: this.defaultRoute, params: {} };
    }

    const { route, params } = routeMatch;
    return this.resolveRoute(route, params, domainName, routingInfo, redirectTo);
  }

  private resolveRedirection = (url: string, domainName: string, routingInfo: IRoutingInfo):
    IResolveRouteResult => {
    const routeMatch = this.findRouteByUrl(url, domainName);
    return this.resolveRouteByPathName(routeMatch, domainName, routingInfo, url);
  }

  private resolveRoute = (
    route: IRoute,
    params: IAnyObject,
    domainName: string,
    routingInfo: IRoutingInfo,
    redirectTo: string = '',
  ): IResolveRouteResult => {
    const { lockedRoutes, currentCheckpointUrl, visitedRoutes } = routingInfo;

    if (!route.alwaysAccessibleRoute) {

      if (lockedRoutes[route.path]) {
        return this.resolveRedirection(currentCheckpointUrl, domainName, routingInfo);
      }

      if (route.requiredVisitedRoute) {
        const isRouterAvailable = visitedRoutes[route.requiredVisitedRoute]
          && (!route.checkIsAvailable || route.checkIsAvailable(this.getState()));

        if (!isRouterAvailable) {
          const requiredRoute = getRouteByPath(this.routes, route.requiredVisitedRoute, domainName);
          return this.resolveRoute(requiredRoute, {}, domainName, routingInfo, requiredRoute.path);
        }
      }
    }

    if (route.beforeRoute) {
      let processRouteResult: any = route.beforeRoute(this.getState, this.dispatch)(params);
      if (typeof processRouteResult === 'function') {
        processRouteResult = processRouteResult();
      }
      const redirectRequired = typeof processRouteResult === 'object' && processRouteResult instanceof UrlProvider;
      if (redirectRequired) {
        return this.resolveRedirection(processRouteResult.url, (processRouteResult as MultiDomainsUrlProvider).predefinedDomainName || domainName, routingInfo);
      }
    }

    return { route, params, redirectTo };
  }

}

export default RouteResolver;
