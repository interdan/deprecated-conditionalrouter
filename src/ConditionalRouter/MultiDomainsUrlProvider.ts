import UrlProvider from './UrlProvider';
import { TNavigate } from './types';
import { getUseHttpsForMultiDomainsApp } from './globalConfig';

const DOMAIN_NAME_PARAMETER = 'domainName';

const removeDomainNameParameter = (baseUrl) => {
  if (!baseUrl) return baseUrl;

  const domainNamePosition = baseUrl.search(/[\?,&]domainName=/);
  if (domainNamePosition === -1) return baseUrl;

  let result = baseUrl.substring(0, domainNamePosition + 1);

  let splitterHasBeenFound = false;
  const domainNameParameterSubStringLength = `&${DOMAIN_NAME_PARAMETER}=`.length;
  for (let charIndex = domainNamePosition + domainNameParameterSubStringLength; charIndex < baseUrl.length; charIndex += 1) {
    const charValue = baseUrl[charIndex];

    if (!splitterHasBeenFound) {
      splitterHasBeenFound = charValue === '&';
      continue;
    }

    result += charValue;
  }

  return result.replace(/(\?|&)$/, '').replace('?&', '?');
};

const addDomainAsQueryParameter = (baseUrl: string, domainName: string): string => {
  const url = removeDomainNameParameter(baseUrl);

  const urlSuffix: string = url.indexOf('?') < 0 ? '?' : '&';
  return `${url}${urlSuffix}${DOMAIN_NAME_PARAMETER}=${domainName}`;
};

const setDomainAsDomainName = (baseUrl: string, domainName: string): string => {
  const isCurrentDomainNameTheSame = domainName.toLowerCase() === window.location.hostname;

  return isCurrentDomainNameTheSame ?
    baseUrl :
    `http${getUseHttpsForMultiDomainsApp() ? 's' : ''}://${domainName}${baseUrl}`;
};

class MultiDomainsUrlProvider extends UrlProvider {
  public predefinedDomainName: string;
  private getCurrentDomain: () => string;
  private isMultiDomainEmulating: boolean;

  constructor(
    url: string, navigate: TNavigate, domainName: string,
    isMultiDomainEmulating: boolean, getCurrentDomain: () => string,
  ) {
    super(url, navigate);

    this.predefinedDomainName = domainName;
    this.getCurrentDomain = getCurrentDomain;
    this.isMultiDomainEmulating = isMultiDomainEmulating;
  }

  get url(): string {
    const domainName: string = this.predefinedDomainName || this.getCurrentDomain();

    const resolveUrl = this.isMultiDomainEmulating ? addDomainAsQueryParameter : setDomainAsDomainName;

    return resolveUrl(this.urlValue, domainName);
  }

}

export default MultiDomainsUrlProvider;
