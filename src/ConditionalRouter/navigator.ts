import { TNavigate, TGetState, TGetCurrentDomainName, IRoute, INavigator, IUrlProvider, TNavigatorValue } from './types';
import { DOMAIN_NAME_QUERY_PARAMETER } from './consts';
import getQueryParameterByName from './utils/getQueryParameterByName';

let getStateFunction: TGetState;
let multiDomainEmulating = false;
let hashRouteMode = false;

const navigateToolProvider = {
  // tslint:disable-next-line: no-empty
  navigate: (path: string) => { },
};

export const navigate: TNavigate = url => navigateToolProvider.navigate(url);

export const getCurrentDomainNameForMultiDomainsApp: TGetCurrentDomainName = (
  search: string = '',
  multiDomains: boolean = false,
  defaultRouteDomainName: string = '',
  isMultiDomainEmulating?: boolean,
  isHashRouteModeUsed?: boolean,
): string => {
  if (!multiDomains) return '';

  const _isMultiDomainEmulating: boolean = typeof isMultiDomainEmulating === 'boolean' ?
    isMultiDomainEmulating :
    multiDomainEmulating;

  // if production or stage we already have a real hostname in url
  const result = (!_isMultiDomainEmulating && window.location.hostname) ||

    // if develop mode and not the very first page we have to have it as query parameter
    getQueryParameterByName(DOMAIN_NAME_QUERY_PARAMETER, typeof isHashRouteModeUsed === 'boolean' ? isHashRouteModeUsed : hashRouteMode, search) ||

    // if develop and root - return domain from default (the only one) route
    defaultRouteDomainName;

  if (!result) {
    throw new Error("Route config error: default route isn't defined for multi domains App!");
  }

  return (result || '').toLowerCase();
};

type TGetDefaultRoute = () => IRoute | null;
let getDefaultRoute: TGetDefaultRoute = () => null;

const navigatorValues = new Map<string, Map<string, TNavigatorValue>>();
const defaultNavigator: Map<string, TNavigatorValue> = new Map();

const getFirstValueOfMap = <TValue>(map: Map<any, TValue>): TValue => map.values().next().value;

const resolveRouteForDomain = (sameNameRouteValues: Map<string, TNavigatorValue> | undefined, domainName: string): TNavigatorValue | null => {
  let result: TNavigatorValue | null = null;
  if (sameNameRouteValues && sameNameRouteValues.size) {
    if (sameNameRouteValues.size > 1) {
      const currentDomain = domainName ? domainName : getCurrentDomainNameForMultiDomainsApp('', true);
      result = sameNameRouteValues.get(currentDomain) || null;
    }
    if (!result) {
      result = getFirstValueOfMap(sameNameRouteValues);
    }
  }
  return result;
};

export const navigatorProto: INavigator = {
  navigateToPreviousRoute: () => {
    const { previousUrl, url } = getStateFunction().routingInfo;

    if (previousUrl && previousUrl !== url) {
      navigateToolProvider.navigate(previousUrl);
    } else {
      const defaultRoute: IRoute = getDefaultRoute() as IRoute;
      navigateToolProvider.navigate(defaultRoute ? defaultRoute.path : '/');
    }
  },

  get default(): TNavigatorValue | null { return resolveRouteForDomain(defaultNavigator, ''); },

  currentNavigator: '',
  // null as unknown as IUrlProvider,

  getCurrent() {
    return this[this.currentNavigator as any] as any;
  },

  setCurrent(routeName: string) {
    this.currentNavigator = routeName;
  },

  useDomain(domainName: string) {
    if (!proxyIsSupported) {
      throw new Error('"useDomain" method is available ONLY for browser with ES6 proxy support!');
    }

    const proxyTarget = {};
    return new Proxy(proxyTarget, {
      get(trapTarget, key: string) {
        return resolveNavigatorValue(key, domainName.toLowerCase());
      },
    });
  },
};

const startNavigatorProtoKeys = new Set<string>(Object.keys(navigatorProto));

const proxyIsSupported = typeof Proxy === 'function';

const resolveNavigatorValue = (
  key: string, domainName: string = '',
): TNavigatorValue | null => resolveRouteForDomain(navigatorValues.get(key), domainName);

export const navigator = proxyIsSupported
  ? new Proxy(navigatorProto, {
    get(trapTarget, key: string, receiver) {
      if (startNavigatorProtoKeys.has(key)) {
        return Reflect.get(trapTarget, key, receiver);
      }

      return resolveNavigatorValue(key);
    },
  })
  : navigatorProto;

export const addNavigatorValue = (
  navigatorKey: string,
  domainName: string,
  navigatorValue: TNavigatorValue,
  isDefaultRoute: boolean,
) => {
  if (proxyIsSupported) {
    let sameNameValues = navigatorValues.get(navigatorKey);
    if (!sameNameValues) {
      sameNameValues = new Map<string, TNavigatorValue>();
      navigatorValues.set(navigatorKey, sameNameValues);
    }

    sameNameValues.set(domainName, navigatorValue);
  } else {
    navigator[navigatorKey] = navigatorValue;
  }

  if (isDefaultRoute) {
    defaultNavigator.set(domainName, navigatorValue);
  }
};

export const initNavigator = (
  _navigate: TNavigate,
  getState: TGetState,
  _getDefaultRoute: TGetDefaultRoute,
  _multiDomainEmulating: boolean,
  _hashRouteMode: boolean,
) => {
  getStateFunction = getState;
  navigateToolProvider.navigate = _navigate;
  getDefaultRoute = _getDefaultRoute;
  multiDomainEmulating = _multiDomainEmulating;
  hashRouteMode = _hashRouteMode;

  // clear navigator (just for better support HMR)
  const propertiesToRemove: string[] = Object.keys(navigator).filter(key => !startNavigatorProtoKeys.has(key));

  propertiesToRemove.forEach(propertyNameToRemove => {
    delete navigator[propertyNameToRemove];
  });
  navigatorValues.clear();
  defaultNavigator.clear();
};
