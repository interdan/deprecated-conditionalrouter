import React, { ComponentType } from 'react';
import { ReactReduxContext } from 'react-redux';
import { IStoreRequiredProps } from './types';

const withReduxContext = (Enhanceable: ComponentType<IStoreRequiredProps>) => {
  return (props: any) => {
    const { Consumer } = props.context || ReactReduxContext;

    return (
      <Consumer>
        {({ store }: any) => <Enhanceable store={store} {...props} />}
      </Consumer>
    );
  };
};

export default withReduxContext;
