import ISmartphone from '../types/ISmartphone';
import IMenuOption from '../types/IMenuOption';

export default function buildOptionsMenu(smartphones: ISmartphone[], propertyName: string): IMenuOption[] {
  const resultOptions: IMenuOption[] = [];
  const addedOptions = new Set<string>();
  smartphones.forEach((smartphone: ISmartphone) => {
    const { id, [propertyName]: propertyValue } = smartphone as any;

    if (!addedOptions.has(propertyValue)) {

      resultOptions.push({ id, title: propertyValue });

      addedOptions.add(propertyValue);
    }
  });

  return resultOptions;
}
