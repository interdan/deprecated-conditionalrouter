import { bindActionCreators, ActionCreatorsMapObject, Dispatch } from 'redux';

const bindActions = <A, M extends ActionCreatorsMapObject<A>>(actionCreators: M) => (dispatch: Dispatch) => (
  bindActionCreators(actionCreators, dispatch)
);

export default bindActions;
