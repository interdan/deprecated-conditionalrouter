import { throttle } from 'lodash';
import IState from '../types/IState';

const LOCAL_STORAGE_STATE_KEY = 'state';

const getStorageKey = (domainName: string): string => (
  domainName ? `${LOCAL_STORAGE_STATE_KEY} of ${domainName}` : LOCAL_STORAGE_STATE_KEY
);

export const restoreStateFromLocalStorage = (domainName: string): IState | undefined => {
  try {
    const serializedState = window.localStorage.getItem(getStorageKey(domainName));
    if (!serializedState) {
      return undefined;
    }
    return JSON.parse(serializedState);
  } catch (e) {
    return undefined;
  }
};

export const saveStateToLocalStorage = throttle(
  (state: object, domainName: string): void => {
    try {
      const serializedState = JSON.stringify(state);
      window.localStorage.setItem(getStorageKey(domainName), serializedState);
    } catch (e) {
      // ignore
    }
  },
  1000,
);
