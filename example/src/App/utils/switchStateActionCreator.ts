import { restoreStateFromLocalStorage } from './persistentState';
import { restoreState } from '../store/root/actionCreators';

export default function switchStateActionCreator(domainName: string) {
  const restoredState = restoreStateFromLocalStorage(domainName);
  return restoreState(restoredState);
}
