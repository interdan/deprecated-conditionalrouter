import { getCurrentDomainNameForMultiDomainsApp } from '@interdan/conditional-router';

import { BUY_A_NEW_PHONE_MAIN_DOMAIN } from '../config/domains';
import { isDevelopEnv, isStageEnv } from '../config/envMocks';

// is up to you how to cinfigure this value, but the feature
// created for development mode primarily
export const isMultiDomainEmulating: boolean = isDevelopEnv || isStageEnv;
export const isHashRouteModeUsed: boolean = true;

const SUPPORT_MULTI_DOMAINS = true;

export const getCurrentDomainName = (): string => getCurrentDomainNameForMultiDomainsApp(
  '',
  SUPPORT_MULTI_DOMAINS,
  BUY_A_NEW_PHONE_MAIN_DOMAIN,
  isMultiDomainEmulating,
  isHashRouteModeUsed,
);
