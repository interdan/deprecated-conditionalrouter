import { getCurrentDomainName } from './currentDomain';
import { AppleBrand, SamsungBrand } from '../config/brands';
import { BUY_A_NEW_IPHONE_DOMAIN, BUY_A_NEW_SAMSUNG_DOMAIN } from '../config/domains';

export default function getCurrentBrand() {
  const domainName = getCurrentDomainName();
  switch (domainName.toLowerCase()) {
    case BUY_A_NEW_IPHONE_DOMAIN.toLowerCase():
      return AppleBrand;

    case BUY_A_NEW_SAMSUNG_DOMAIN.toLowerCase():
      return SamsungBrand;
  }

  return '';
}
