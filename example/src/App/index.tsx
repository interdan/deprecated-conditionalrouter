import { ConditionalRouter } from '@interdan/conditional-router';
import React from 'react';
import { Provider } from 'react-redux';
import history from './sharedHistory';
import './styles.scss';
import store from './store';
import { Dispatch } from 'redux';
import setRoutingRules from './routingRules';
import { isMultiDomainEmulating, isHashRouteModeUsed } from './utils/currentDomain';
import { isProduction, isStageEnv } from './config/envMocks';
import IRouteCustomSettings from './types/IRouteCustomSettings';
import switchStateActionCreator from './utils/switchStateActionCreator';
import IState from './types/IState';

// 'dispatch' & 'getState' aren't used here, but you can get state value and dispatch actions
// 'routeParams' is also available here
const onRouteGlobalHandler = (dispatch: Dispatch, getState: () => IState) =>
  (routeParams: any, routeCustomSettings: IRouteCustomSettings) => {
    if (routeCustomSettings && (routeCustomSettings as IRouteCustomSettings).syncSession) {
      // tslint:disable-next-line: no-console
      console.log('sync session');
    }
  };

const oftenHistoryPushDetection = {
  historyMaxLength: 25,
  historyItemsCountToCheckOftenPush: 5,
  minAcceptableMillisecondsOffsetForPushXItemToHistory: 50000,
  notifyTooMuchHistoryPush: (details) => {
    console.log('often history push detected: ', details);
  },
};

class App extends React.Component {

  public render() {
    return (
      <Provider store={store}>
        <ConditionalRouter
          isMultiDomainEmulating={isMultiDomainEmulating}
          useHttpsForMultiDomainsApp={isProduction || isStageEnv}
          onRoute={onRouteGlobalHandler as any}
          setRules={setRoutingRules}
          switchStateActionCreator={switchStateActionCreator}
          history={history}
          hashRouteMode={isHashRouteModeUsed}
          oftenHistoryPushDetection={oftenHistoryPushDetection}
        />
      </Provider>
    );
  }
}

export default App;
