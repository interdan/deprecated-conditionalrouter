interface IMenuOption {
  title: string;
  id: number;
}

export default IMenuOption;
