import IChosenOptionsState from './IChosenOptionsState';
import IGoodsData from './IGoodsData';

interface IState {
  chosenOptions: IChosenOptionsState;
  goodsData: IGoodsData;
}

export default IState;
