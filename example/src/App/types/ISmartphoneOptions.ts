import IMenuOption from './IMenuOption';

interface ISmartphoneOptions {
  options: IMenuOption[];
  chooseOption: (menuOption: IMenuOption) => void;
  chosenValue: string;
}

export default ISmartphoneOptions;
