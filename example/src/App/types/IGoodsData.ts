import ISmartphone from './ISmartphone';

interface IGoodsData {
  smartphones: ISmartphone[];
}

export default IGoodsData;
