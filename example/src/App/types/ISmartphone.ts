interface ISmartphone {
  brand: string;
  screenResolution: string;
  waterProtection: string;
  wirelessCharging: string;
  model: string;
  id: number;
}

export default ISmartphone;
