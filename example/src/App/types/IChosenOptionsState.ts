interface IChosenOptionsState {
  screenResolution: string;
  waterProtection: string;
  wirelessCharging: string;
}

export default IChosenOptionsState;
