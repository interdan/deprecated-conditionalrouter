import { withRouterReducer } from '@interdan/conditional-router';

import { combineReducers } from 'redux';
import { merge } from 'lodash';
import chosenOptionsReducer from '../chosenOptions/reducer';
import goodsDataReducer from '../goodsData/reducer';
import IState from '../../types/IState';
import IAction from '../../types/IAction';
import actionTypes from './actionTypes';

const combinedReducer = combineReducers(withRouterReducer({
  chosenOptions: chosenOptionsReducer,
  goodsData: goodsDataReducer,
}));

const getDefaultState = () => combinedReducer(undefined, { type: '' }) as unknown as IState;

const rootReducer = (state: IState, action: IAction): IState => {
  let resultState: IState;

  switch (action.type) {
    case actionTypes.RESTORE_STATE:
      resultState = merge(getDefaultState(), action.payload || {});
      break;

    case actionTypes.RESET_STATE:
      resultState = getDefaultState();
      break;

    default:
      resultState = combinedReducer(state as unknown as any, action) as unknown as IState;
      break;
  }

  return resultState;
};

export default rootReducer;
