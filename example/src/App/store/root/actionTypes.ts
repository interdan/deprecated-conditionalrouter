enum actionTypes {
  RESTORE_STATE = 'RESTORE_STATE',
  RESET_STATE = 'RESET_STATE',
}

export default actionTypes;
