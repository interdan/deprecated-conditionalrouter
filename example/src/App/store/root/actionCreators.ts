import actionTypes from './actionTypes';
import IAction from '../../types/IAction';
import IState from '../../types/IState';

export const restoreState = (stateValue?: IState): IAction => ({ type: actionTypes.RESTORE_STATE, payload: stateValue });
export const resetState = () => ({ type: actionTypes.RESET_STATE });
