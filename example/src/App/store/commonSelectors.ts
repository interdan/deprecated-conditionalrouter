import IState from '../types/IState';
import ISmartphone from '../types/ISmartphone';
import getCurrentBrand from '../utils/getCurrentBrand';

export const getSmartphones = (state: IState): ISmartphone[] => state.goodsData.smartphones;

export const getSmartphonesForCurrentBrand = (state: IState): ISmartphone[] => {
  const currentBrand = getCurrentBrand();

  return getSmartphones(state)
    .filter(({ brand }) => currentBrand === brand);
};
