import actionTypes from './actionTypes';
import IAction from '../../types/IAction';
import IMenuOption from '../../types/IMenuOption';

export const setResolutionValue = (resolution: IMenuOption): IAction => ({
  type: actionTypes.SET_RESOLUTION,
  payload: resolution.title,
});

export const setWaterProtection = (waterProtection: IMenuOption): IAction => ({
  type: actionTypes.SET_WATER_PROTECTION,
  payload: waterProtection.title,
});

export const setWirelessCharging = (wirelessCharging: IMenuOption): IAction => ({
  type: actionTypes.SET_WIRELESS_CHARGING,
  payload: wirelessCharging.title,
});
