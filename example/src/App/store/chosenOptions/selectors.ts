import { createSelector } from 'reselect';
import IState from '../../types/IState';
import { getSmartphonesForCurrentBrand } from '../commonSelectors';
import ISmartphone from '../../types/ISmartphone';

export const getWirelessCharging = (state: IState) => state.chosenOptions.wirelessCharging;

export const getWaterProtection = (state: IState) => state.chosenOptions.waterProtection;

export const getScreenResolution = (state: IState) => state.chosenOptions.screenResolution;

export const getChosenModel = createSelector(
  getSmartphonesForCurrentBrand,
  getScreenResolution,
  getWirelessCharging,
  getWaterProtection,
  (smartphones: ISmartphone[], chosenScreenResolution, chosenWirelessCharging, chosenWaterProtection)
    : string => {

    const filteredOptions = smartphones.filter((
      { wirelessCharging, waterProtection, screenResolution },
    ): boolean => (!chosenWirelessCharging || chosenWirelessCharging === wirelessCharging)
    && (!chosenWaterProtection || chosenWaterProtection === waterProtection)
      && (!chosenScreenResolution || chosenScreenResolution === screenResolution));

    return filteredOptions.length ? filteredOptions[0].model : '';
  },
);
