import IAction from '../../types/IAction';
import actionTypes from './actionTypes';
import IChosenOptionsState from '../../types/IChosenOptionsState';

const getDefaultState = (): IChosenOptionsState => ({
  screenResolution: '',
  waterProtection: '',
  wirelessCharging: '',
});

const chosenOptionsReducer = (state: IChosenOptionsState = getDefaultState(), action: IAction) => {
  switch (action.type) {
    case actionTypes.SET_RESOLUTION:
      return { ...state, screenResolution: action.payload };

    case actionTypes.SET_WATER_PROTECTION:
      return { ...state, waterProtection: action.payload };

    case actionTypes.SET_WIRELESS_CHARGING:
      return { ...state, wirelessCharging: action.payload };

    default:
      return state;
  }
};

export default chosenOptionsReducer;
