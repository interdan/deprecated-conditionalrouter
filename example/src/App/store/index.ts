import { applyMiddleware, createStore } from 'redux';
import { createLogger } from 'redux-logger';
import { merge } from 'lodash-core';
import { composeWithDevTools } from 'redux-devtools-extension';
import rootReducer from './root';
import { saveStateToLocalStorage, restoreStateFromLocalStorage } from '../utils/persistentState';
import { getCurrentDomainName } from '../utils/currentDomain';
import IState from '../types/IState';

const middleware = applyMiddleware(createLogger());

let savedState = restoreStateFromLocalStorage(getCurrentDomainName());

if (savedState) {
  const defaultState = rootReducer(undefined as any, { type: 'any-type' });
  savedState = merge(defaultState, savedState);
}

const store = createStore(
  rootReducer as any,
  savedState,
  composeWithDevTools(),
  // composeWithDevTools(middleware),
);

store.subscribe(() => {
  const state = store.getState();

  saveStateToLocalStorage(state, getCurrentDomainName());
});

export default store;
