import { createSelector } from 'reselect';
import ISmartphone from '../../types/ISmartphone';
import IMenuOption from '../../types/IMenuOption';
import buildOptionsMenu from '../../utils/buildOptionsMenu';
import { getScreenResolution, getWaterProtection } from '../chosenOptions/selectors';
import { getSmartphonesForCurrentBrand } from '../commonSelectors';

export const getScreenResolutionOptions = createSelector(
  getSmartphonesForCurrentBrand,
  (smartphones: ISmartphone[]): IMenuOption[] => buildOptionsMenu(smartphones, 'screenResolution'),
);

const getFilteredSmartphoneByScreenResolution = createSelector(
  getSmartphonesForCurrentBrand,
  getScreenResolution,
  (smartphones: ISmartphone[], chosenResolution)
    : ISmartphone[] => smartphones
      .filter(({ screenResolution }) => chosenResolution === screenResolution),
);

export const getWaterProtectionOptions = createSelector(
  getFilteredSmartphoneByScreenResolution,
  (smartphones: ISmartphone[]): IMenuOption[] => buildOptionsMenu(smartphones, 'waterProtection'),
);

const getFilteredSmartphoneByScreenResolutionWaterProtection = createSelector(
  getFilteredSmartphoneByScreenResolution,
  getWaterProtection,
  (smartphones: ISmartphone[], chosenWaterProtection)
    : ISmartphone[] => smartphones
      .filter(({ waterProtection }) => chosenWaterProtection === waterProtection),
);

export const getWirelessChargingOptions = createSelector(
  getFilteredSmartphoneByScreenResolutionWaterProtection,
  (smartphones: ISmartphone[]): IMenuOption[] => buildOptionsMenu(smartphones, 'wirelessCharging'),
);
