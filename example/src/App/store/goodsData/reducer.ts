import IAction from '../../types/IAction';
import IGoodsData from '../../types/IGoodsData';
import { getSmartphonesMockData } from '../../mockDataGenerator';

const getDefaultState = (): IGoodsData => ({
  smartphones: getSmartphonesMockData(),
});

const goodsDataReducer = (state: IGoodsData = getDefaultState(), action: IAction) => {
  return state;
};

export default goodsDataReducer;
