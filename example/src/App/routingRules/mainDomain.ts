import { TAddRoutesType } from '@interdan/conditional-router';
import StartView from '../components/BuySmartphoneStartPage';
import { BUY_A_NEW_PHONE_MAIN_DOMAIN } from '../config/domains';

const domainName = BUY_A_NEW_PHONE_MAIN_DOMAIN;

export default function setMainDomainRoutingRules(addRoutes: TAddRoutesType, navigator: any) {
  addRoutes([
    {
      StartView,
      domainName,
      path: '/',
      useAsDefault: true,
    },
  ]);
}
