import setMainDomainRoutingRules from './mainDomain';
import { TAddRoutesType } from '@interdan/conditional-router';
import ResolutionChooser from '../components/ResolutionChooser';
import WaterProtectionChooser from '../components/WaterProtectionChooser';
import WirelessChargingChooser from '../components/WirelessChargingChooser';
import Checkout from '../components/Checkout';
import CongratulationPage from '../components/CongratulationPage';
import BrandContainer from '../components/BrandContainer';
import { BUY_A_NEW_IPHONE_DOMAIN, BUY_A_NEW_SAMSUNG_DOMAIN } from '../config/domains';
import IRouteCustomSettings from '../types/IRouteCustomSettings';
import { getWirelessChargingOptions } from '../store/goodsData/selectors';
import { setResolutionValue } from '../store/chosenOptions/actionCreators';
import { Dispatch } from 'redux';

const commonFlowDomains = [BUY_A_NEW_IPHONE_DOMAIN, BUY_A_NEW_SAMSUNG_DOMAIN];

const synkSessionParameter: IRouteCustomSettings = { syncSession: true };

export default function setRoutingRules(addRoutes: TAddRoutesType, navigator: any) {

  setMainDomainRoutingRules(addRoutes, navigator);

  commonFlowDomains.forEach((domainName: string) => {
    addRoutes(BrandContainer, [
      {
        domainName,
        ResolutionChooser,
        requiredStep: true,
        path: '/ScreenResolutionChooser?predefinedResolution=:predefinedResolution',
        onRoute: (dispatch: Dispatch, getState) => ({ predefinedResolution }: any) => {
          if (predefinedResolution) {
            dispatch(setResolutionValue({ title: predefinedResolution, id: 0 }));
          }
        },
      },
      { WaterProtectionChooser, skipWhenBackToPrevious: true },
      {
        WirelessChargingChooser,
        skipWhenBackToPrevious: true,
        beforeRoute: getState => (routeParameters: any) => {
          const wirelessChargingOptions = getWirelessChargingOptions(getState() as any);

          // dumb checking for testing purposes only
          const wirelessNotSupported = wirelessChargingOptions.length <= 1;
          return wirelessNotSupported ? navigator.Checkout() : null;
        },
      },
    ]);

    addRoutes([
      {
        Checkout,
        customSettings: synkSessionParameter,
        requiredStep: true,
      },
      {
        CongratulationPage,
        customSettings: synkSessionParameter,
        routesToLockOnFirstLoad: { Checkout, WirelessChargingChooser, WaterProtectionChooser, ResolutionChooser },
      },
    ]);
  });

}
