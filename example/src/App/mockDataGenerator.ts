import ISmartphone from './types/ISmartphone';
import { AppleBrand, SamsungBrand } from './config/brands';

export const brands = [AppleBrand, SamsungBrand];

export const biggestScreenResolution = '2688x1242';
const withoutWirelessChargingScreenResolution = '1136x640';
const resolutionsOptions = [biggestScreenResolution, '2436x1125', withoutWirelessChargingScreenResolution];
const resolutionsOptionNames = {
  [AppleBrand]: ['XS', 'X', 'SE'],
  [SamsungBrand]: ['Giant', 'Standard', 'Small'],
} as any;

const waterProtectionOptions = ['Not protected', 'IP67', 'IP68'];

const wirelessChargingOptions = ['Not supported', '2 hours', 'Ultra fast: 30 min'];
const wirelessChargingOptionNames = ['Not supported', 'Wireless charging', 'Fast wireless charging'];

const buildModelName = (
  brand: string, screenResolution: string, waterProtection: string, wirelessCharging: string,
): string => {

  let result = brand === AppleBrand ? 'iPhone' : brand;

  result += ` ${resolutionsOptionNames[brand][resolutionsOptions.indexOf(screenResolution)]}`;

  if (waterProtectionOptions.indexOf(waterProtection) > 0) {
    result += ` ${waterProtection}`;
  }

  const wirelessChargingOptionIndex = wirelessChargingOptions.indexOf(wirelessCharging);
  if (wirelessChargingOptionIndex > 0) {
    result += ` ${wirelessChargingOptionNames[wirelessChargingOptionIndex]}`;
  }

  return result;
};

export const getSmartphonesMockData = (): ISmartphone[] => {
  const result = [] as ISmartphone[];
  let idMockGenerator = 0;

  brands.forEach((brand: string) => resolutionsOptions
    .forEach((screenResolution: string) => waterProtectionOptions
      .forEach((waterProtection: string) => wirelessChargingOptions
        .forEach((wirelessCharging: string) => {

          if (brand === 'Apple'
            && screenResolution === withoutWirelessChargingScreenResolution
            && wirelessChargingOptions.indexOf(wirelessCharging) > 0
          ) return;

          idMockGenerator += 1;

          result.push({
            brand,
            wirelessCharging,
            waterProtection,
            screenResolution,
            id: idMockGenerator,
            model: buildModelName(brand, screenResolution, waterProtection, wirelessCharging),
          } as ISmartphone);

        }))));

  return result;
};
