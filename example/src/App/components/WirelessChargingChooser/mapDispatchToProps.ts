import bindActions from '../../utils/bindActions';
import { setWirelessCharging } from '../../store/chosenOptions/actionCreators';

export default bindActions({
  chooseOption: setWirelessCharging,
});
