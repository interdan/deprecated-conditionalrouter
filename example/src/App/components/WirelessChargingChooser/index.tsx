import React, { ReactNode } from 'react';
import mapStateToProps from './mapStateToProps';
import mapDispatchToProps from './mapDispatchToProps';
import { connect } from 'react-redux';
import navigator from '@interdan/conditional-router';
import ISmartphoneOptions from '../../types/ISmartphoneOptions';
import OptionsMenu from '../OptionsMenu';

const WirelessChargingChooser: any = (props: ISmartphoneOptions): ReactNode => (
  <OptionsMenu {...props} title="Do you need wireless charging?" nextPageUrl={navigator.Checkout} />
);

export default connect(mapStateToProps, mapDispatchToProps)(WirelessChargingChooser);
