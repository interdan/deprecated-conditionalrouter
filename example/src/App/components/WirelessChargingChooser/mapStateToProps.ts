import { getWirelessChargingOptions } from '../../store/goodsData/selectors';
import IState from '../../types/IState';
import { getWirelessCharging } from '../../store/chosenOptions/selectors';

const mapStateToProps = (state: IState) => ({
  options: getWirelessChargingOptions(state),
  chosenValue: getWirelessCharging(state),
});

export default mapStateToProps;
