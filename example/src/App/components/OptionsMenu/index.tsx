import React from 'react';
import { NavLink } from '@interdan/conditional-router';

import IMenuOption from '../../types/IMenuOption';
import ISmartphoneOptions from '../../types/ISmartphoneOptions';

import './styles.scss';

interface IOptionsMenuProps extends ISmartphoneOptions {
  title: string;
  nextPageUrl: any;
}

const OptionsMenu = ({ chosenValue, title, options, chooseOption, nextPageUrl }: IOptionsMenuProps) => {
  return (
    <div className="options-menu">
      <div className="title">{title}</div>
      <div className="options">
        {options.map((option: IMenuOption) => (
          <button
            key={option.id}
            onClick={chooseOption.bind(null, option)}
            className={chosenValue === option.title ? 'chosen' : undefined}>
            {option.title}
          </button>
        ))}
      </div>
      <div className="proceed">
        <NavLink
          to={nextPageUrl}
          disabled={!chosenValue}
          className={chosenValue ? undefined : 'disabled'}
        >
          Next
        </NavLink>
      </div>
    </div>
  );
};

export default OptionsMenu;
