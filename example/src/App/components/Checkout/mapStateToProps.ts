import IState from '../../types/IState';
import { getWaterProtection, getWirelessCharging, getScreenResolution, getChosenModel } from '../../store/chosenOptions/selectors';

const mapStateToProps = (state: IState) => ({
  waterProtection: getWaterProtection(state),
  wirelessCharging: getWirelessCharging(state),
  screenResolution: getScreenResolution(state),
  model: getChosenModel(state),
});

export default mapStateToProps;
