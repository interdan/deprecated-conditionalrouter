import React, { ReactNode } from 'react';
import mapStateToProps from './mapStateToProps';
import { connect } from 'react-redux';
import navigator from '@interdan/conditional-router';

import './styles.scss';

interface ICheckoutProps {
  waterProtection: string;
  wirelessCharging: string;
  screenResolution: string;
  model: string;
}

const Checkout: any = (
  { model, waterProtection, wirelessCharging, screenResolution }: ICheckoutProps,
): ReactNode => (
    <div className="checkout">
      <div className="chosen-phone-info">
        <h2>Your chosen phone is: "{model}"</h2>
        <ul>
          <li>Screen resolution: {screenResolution}</li>
          <li>Water protection: {waterProtection}</li>
          <li>Wireless charging: {wirelessCharging}</li>
        </ul>
      </div>

      <div className="buttons">
        <button className="submit" onClick={navigator.CongratulationPage().go}>Submit</button>
        <button className="submit" onClick={navigator.navigateToPreviousRoute}>Back to configurator</button>
      </div>
    </div>
  );

export default connect(mapStateToProps)(Checkout);
