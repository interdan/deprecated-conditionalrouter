import { getWaterProtectionOptions } from '../../store/goodsData/selectors';
import IState from '../../types/IState';
import { getWaterProtection } from '../../store/chosenOptions/selectors';

const mapStateToProps = (state: IState) => ({
  options: getWaterProtectionOptions(state),
  chosenValue: getWaterProtection(state),
});

export default mapStateToProps;
