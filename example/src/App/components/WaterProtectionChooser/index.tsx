import React, { ReactNode } from 'react';
import mapStateToProps from './mapStateToProps';
import mapDispatchToProps from './mapDispatchToProps';
import { connect } from 'react-redux';
import navigator from '@interdan/conditional-router';
import ISmartphoneOptions from '../../types/ISmartphoneOptions';
import OptionsMenu from '../OptionsMenu';

const WaterProtectionChooser: any = (props: ISmartphoneOptions): ReactNode => (
  <OptionsMenu {...props} title="What water level of water protection do you need?" nextPageUrl={navigator.WirelessChargingChooser} />
);

export default connect(mapStateToProps, mapDispatchToProps)(WaterProtectionChooser);
