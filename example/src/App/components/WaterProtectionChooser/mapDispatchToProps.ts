import bindActions from '../../utils/bindActions';
import { setWaterProtection } from '../../store/chosenOptions/actionCreators';

export default bindActions({
  chooseOption: setWaterProtection,
});
