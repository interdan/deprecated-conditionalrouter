import bindActions from '../../utils/bindActions';
import { setResolutionValue } from '../../store/chosenOptions/actionCreators';

export default bindActions({
  chooseOption: setResolutionValue,
});
