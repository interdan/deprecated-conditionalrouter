import { getScreenResolutionOptions } from '../../store/goodsData/selectors';
import IState from '../../types/IState';
import { getScreenResolution } from '../../store/chosenOptions/selectors';

const mapStateToProps = (state: IState) => ({
  options: getScreenResolutionOptions(state),
  chosenValue: getScreenResolution(state),
});

export default mapStateToProps;
