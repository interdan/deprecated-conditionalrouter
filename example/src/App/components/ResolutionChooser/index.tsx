import React, { ReactNode } from 'react';
import mapStateToProps from './mapStateToProps';
import mapDispatchToProps from './mapDispatchToProps';
import { connect } from 'react-redux';
import navigator from '@interdan/conditional-router';
import ISmartphoneOptions from '../../types/ISmartphoneOptions';
import OptionsMenu from '../OptionsMenu';

const ResolutionChooser: any = (props: ISmartphoneOptions): ReactNode => (
  <OptionsMenu
    {...props}
    title="Choose desired screen resolution"
    nextPageUrl={navigator.WaterProtectionChooser}
  />
);

export default connect(mapStateToProps, mapDispatchToProps)(ResolutionChooser);
