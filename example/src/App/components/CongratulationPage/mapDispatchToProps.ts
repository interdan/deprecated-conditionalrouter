import bindActions from '../../utils/bindActions';
import { resetState } from '../../store/root/actionCreators';

export default bindActions({
  resetState,
});
