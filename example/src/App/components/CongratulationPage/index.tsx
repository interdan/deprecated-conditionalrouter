import React, { ReactNode } from 'react';
import mapStateToProps from './mapStateToProps';
import mapDispatchToProps from './mapDispatchToProps';
import { connect } from 'react-redux';
import navigator from '@interdan/conditional-router';

import './styles.scss';

interface ICongratulationPageProps {
  waterProtection: string;
  wirelessCharging: string;
  screenResolution: string;
  model: string;
  resetState: () => void;
}

const CongratulationPage: any = (
  { model, waterProtection, wirelessCharging, screenResolution, resetState }: ICongratulationPageProps,
): ReactNode => {

  const onStartANewSession = () => {
    resetState();
    navigator.default().go();
  };

  return (
    <div className="congratulation">
      <h1>Congratulation, you just have ordered a new "{model}"</h1>
      <div className="chosen-phone-info">
        Smartphone details:
        <ul>
          <li>Screen resolution: {screenResolution}</li>
          <li>Water protection: {waterProtection}</li>
          <li>Wireless charging: {wirelessCharging}</li>
        </ul>
      </div>
      <div className="message">
        Our manager will contact you today!
      </div>
      <button onClick={onStartANewSession} className="start-new-session">Order one more smartphone</button>
    </div>
  );
};

export default connect(mapStateToProps, mapDispatchToProps)(CongratulationPage);
