import React from 'react';
import navigator from '@interdan/conditional-router';

import './styles.scss';
import { BUY_A_NEW_IPHONE_DOMAIN, BUY_A_NEW_SAMSUNG_DOMAIN } from '../../config/domains';
import { biggestScreenResolution as predefinedResolution } from '../../mockDataGenerator';

const BuySmartphoneStartPage = () => {
  return (
    <div className="buy-smartphone">
      <header>Buy a new Smartphone!</header>
      <h2>What operating system do you prefer?</h2>
      <div className="ios-options">
        <button onClick={navigator.useDomain(BUY_A_NEW_IPHONE_DOMAIN).ResolutionChooser().go}>iOS</button>
        <button onClick={navigator.useDomain(BUY_A_NEW_SAMSUNG_DOMAIN).ResolutionChooser().go}>Android</button>
      </div>

      <button
        className="predefined-screen"
        onClick={navigator.useDomain(BUY_A_NEW_IPHONE_DOMAIN).ResolutionChooser({ predefinedResolution }).go}
      >
        iPhone with the biggest screen
      </button>
    </div>
  );
};

export default BuySmartphoneStartPage;
