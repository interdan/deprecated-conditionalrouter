import IState from '../../types/IState';
import getCurrentBrand from '../../utils/getCurrentBrand';

const mapStateToProps = (state: IState) => ({
  brand: getCurrentBrand(),
});

export default mapStateToProps;
