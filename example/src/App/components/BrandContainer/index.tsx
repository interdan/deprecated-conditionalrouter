import React, { ReactNode } from 'react';
import mapStateToProps from './mapStateToProps';
import { connect } from 'react-redux';

import './styles.scss';

interface IBrandContainerProps {
  brand: string;
  children: ReactNode;
}

const BrandContainer: any = ({ brand, children }: IBrandContainerProps,
): ReactNode => (
    <div className="brand-container">
      <header>Configure your new {brand} smartphone</header>
      <div>
        {children}
      </div>
    </div>
  );

export default connect(mapStateToProps)(BrandContainer);
