const webpack = require('webpack');
const HtmlWebpackPlugin = require('html-webpack-plugin');
const MiniCssExtractPlugin = require('mini-css-extract-plugin');
const OpenBrowserPlugin = require('open-browser-webpack-plugin');
const TSLintPlugin = require('tslint-webpack-plugin');

const autoprefixer = require('autoprefixer');
const { resolve, join } = require('path');

const PORT = 3000;
const url = `http://localhost:${PORT}`;
const SRC = resolve(__dirname, 'src');

const config = {
  mode: 'development',
  devtool: 'cheap-module-source-map',

  entry: [
    'react-hot-loader/patch',
    'react',
    'react-dom',
    `webpack-dev-server/client?${url}`,
    'webpack/hot/only-dev-server',
    './index.tsx',
  ],

  output: {
    filename: 'bundle.js',
    path: resolve(__dirname, 'dist'),
    publicPath: '/',
    devtoolModuleFilenameTemplate: ({ absoluteResourcePath }) => `file:///${encodeURI(absoluteResourcePath)}`,
    devtoolFallbackModuleFilenameTemplate: ({ absoluteResourcePath, hash }) => `file:///${encodeURI(absoluteResourcePath)}?${hash}`,
  },

  context: SRC,

  devServer: {
    port: PORT,
    hot: true,
    contentBase: SRC,
    historyApiFallback: true,
    publicPath: '/',
    overlay: {
      warnings: false,
      errors: true,
    },
    stats: {
      assets: false,
      builtAt: false,
      version: false,
      children: false,
      modules: false,
      hash: false,
    },
  },

  resolve: {
    modules: [SRC, 'node_modules'],
    extensions: ['.js', '.jsx', '.ts', '.tsx'],
    alias: {
      '@interdan/conditional-router': join(SRC, '..', '..', 'src'),
    }
  },

  module: {
    rules: [
      {
        test: /\.[tj]sx?$/,
        exclude: /node_modules/,
        loader: "ts-loader",
      },
      {
        test: /\.scss$/,
        exclude: /node_modules/,
        use: [
          {
            loader: 'css-hot-loader',
          },
          {
            loader: MiniCssExtractPlugin.loader,
          },
          {
            loader: 'css-loader',
            options: {
              sourceMap: true,
            },
          },
          {
            loader: 'postcss-loader',
            options: {
              plugins: [
                autoprefixer({
                  browsers: ['ie >= 8', 'last 4 version'],
                }),
              ],
              sourceMap: true,
            },
          },
          {
            loader: 'sass-loader',
            query: {
              sourceMap: false,
            },
          },
        ],
      },
      {
        test: /\.css$/,
        loader: 'style-loader!css-loader!postcss-loader',
      },
      {
        test: /\.(png|jpg|gif|ico)$/,
        exclude: [join(SRC, 'assets/seo')],
        use: [
          {
            loader: 'url-loader',
            options: {
              limit: 192,
              mimetype: 'image/png',
              name: 'images/[name].[ext]',
            },
          },
        ],
      },
      {
        test: /\.(png|ico)$/,
        exclude: [join(SRC, 'assets/images')],
        include: [join(SRC, 'assets/seo')],
        use: [
          {
            loader: 'url-loader',
            options: {
              limit: 192,
              mimetype: 'image/png',
              name: '[name].[ext]',
            },
          },
        ],
      },
      {
        test: /\.pdf$/,
        use: [
          {
            loader: 'file-loader',
            options: {
              name: '[path][name].[ext]',
            },
          },
        ],
      },
      {
        test: /\.eot(\?v=\d+.\d+.\d+)?$/,
        use: [
          {
            loader: 'file-loader',
            options: {
              name: 'fonts/[name].[ext]',
            },
          },
        ],
      },
      {
        test: /\.woff(2)?(\?v=[0-9]\.[0-9]\.[0-9])?$/,
        use: [
          {
            loader: 'url-loader',
            options: {
              limit: 8192,
              mimetype: 'application/font-woff',
              name: 'fonts/[name].[ext]',
            },
          },
        ],
      },
      {
        test: /\.[ot]tf(\?v=\d+.\d+.\d+)?$/,
        use: [
          {
            loader: 'url-loader',
            options: {
              limit: 8192,
              mimetype: 'application/octet-stream',
              name: 'fonts/[name].[ext]',
            },
          },
        ],
      },
      {
        test: /\.svg(\?v=\d+\.\d+\.\d+)?$/,
        use: [
          {
            loader: 'url-loader',
            options: {
              limit: 2,
              mimetype: 'image/svg+xml',
              name: 'images/[name].[ext]',
            },
          },
        ],
      },
      {
        test: /site\.webmanifest$/,
        loader: 'file-loader',
        options: {
          name: '[name].[ext]',
        },
      },
      {
        test: /browserconfig\.xml$/,
        loader: 'file-loader',
        options: {
          name: '[name].[ext]',
        },
      },
      {
        // TODO: replace with actual file name
        test: /googlef037150eb2571e60\.html$/,
        loader: 'file-loader',
        options: {
          name: '[name].[ext]',
        },
      },
    ],
  },

  plugins: [
    new TSLintPlugin({
      files: ['./src/**/*.ts'],
    }),
    new webpack.IgnorePlugin(/^\.\/locale$/, /moment$/),
    new HtmlWebpackPlugin({
      template: join(SRC, 'index.html'),
      filename: 'index.html',
      inject: 'body',
    }),
    new MiniCssExtractPlugin({
      filename: './styles/style.css',
    }),
    new OpenBrowserPlugin({ url }),
    new webpack.HotModuleReplacementPlugin(),
  ],
};

module.exports = config;
